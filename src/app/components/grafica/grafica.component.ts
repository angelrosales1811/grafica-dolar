import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../../rest-api.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.css']
})
export class GraficaComponent implements OnInit {
  dias: any;
  public lineChartData : Array<any>;
  public lineChartLabels:Array<any> = [];
  
  constructor(private resApi : RestApiService, private datePipe: DatePipe){
    this.dias=[];
    this.lineChartData=[
      {data: [0, 0, 0, 0, 0, 0, 0], label: '$'}
    ];

    var cotizaciones=[];
    var fecha = new Date();
    
    for (let i = 0; i <7; i++){
      cotizaciones.push(fecha.getDate()-i);
    }
    
    cotizaciones.forEach((c:any) => {
    
      this.lineChartLabels.push(c);
    });
    this.lineChartLabels = this.lineChartLabels.reverse();
    
  }
  
   ngOnInit(){
    

  }
  
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData=[
      {data: [0, 0, 0, 0, 0, 0, 0], label: '$'}
    ];
    var data=[];

    this.resApi.getData()
    .then((respuesta : any) =>{
      this.dias= respuesta.USD_MXN;
    })
    .catch(err => console.log('Error',err));
    
    var cotizaciones=[];
    var fecha = new Date();
    var hoy = this.datePipe.transform(fecha, 'yyyy-MM-dd');
    var ant = this.datePipe.transform(fecha.setDate((fecha.getDate()+1)),'yyyy-MM-dd');
    for (let i = 7; i >0; i--){
      cotizaciones.push(this.datePipe.transform(fecha.setDate((fecha.getDate()-1)),'yyyy-MM-dd'));
    }

    cotizaciones.forEach((c:any) => {
      data.push(this.dias[c]);
    });
    _lineChartData[0]["data"] = data.reverse();
    this.lineChartData = _lineChartData;
  }

   
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

}
