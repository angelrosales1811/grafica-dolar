import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  baseURL="https://free.currencyconverterapi.com/api/v6";

  constructor(private _http : HttpClient, private datePipe: DatePipe) { }

  getData(){
    var fecha = new Date();
    console.log(this.datePipe.transform(fecha, 'yyyy-MM-dd'));
    
    var hoy = this.datePipe.transform(fecha, 'yyyy-MM-dd');
    var ant = this.datePipe.transform(fecha.setDate((fecha.getDate()-7)),'yyyy-MM-dd');
    return this._http.get(this.baseURL+"/convert?q=USD_MXN,MXN_USD&compact=ultra&date="+ant+"&endDate="+hoy).toPromise();
  }
}



////convert?q=USD_MXN,MXN_USD&compact=ultra&date=2018-09-10&endDate=2018-09-15